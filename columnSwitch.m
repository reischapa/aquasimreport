function res=columnSwitch(col1,col2,mat)
tmp = mat(:,col1);
res = mat;
res(:,col1) = mat(:,col2);
res(:,col2) = tmp;
endfunction
