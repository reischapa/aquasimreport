function [alpha,beta,gamma]=selectiveCond(x,y)
    cmp1=(5*x/2)
    cmp2=(5*y/3)
    if cmp1>cmp2
        alpha=5*y/3;
        beta=x+y-alpha;
        gamma=0;
    else
        alpha=5*x/2;
        beta=0;
        gamma=x+y-alpha;
    endif   
#ALPHA PARA PHV
#BETA PARA PHB
#GAMMA PARA PH2MV
endfunction
