## Copyright (C) 2015 chapa
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} prep (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: chapa <chapa@chapa-PARTNER>
## Created: 2015-04-25

function [retval,som] = condensationAlgorithm (input1, input2,known = [1])

    input1=[input1;1:columns(input1)];

    input1 = columnSwitch(known(1),columns(input1),input1);

    reference=input1(rows(input1),:);
    referenceLen=columns(input1);
    input1=input1(1:rows(input1)-1,:);

    input1=[input1,zeros(rows(input1),1)];

    input1=rref(input1);

    input1=input1(:,1:columns(input1)-1);

    som=zeros(columns(input1),1);
    
    counter=1;

    for i=1:columns(reference)-length(known)
        if any(input1(:,i))
            som(reference(i),1)=-1*input1(counter,8);
            counter=counter+1;
        else
            som(reference(i),1)=0;
        endif
    endfor
    
    som(known(1),1)=1;

    retval=input2*som;     




endfunction
